#include "TCPListener.h"


SOCKET TCPServer::CreateSocket()
{
  SOCKET sock_server = socket(AF_INET, SOCK_STREAM, 0);
  if (sock_server != INVALID_SOCKET)
  {
    //��������� hint ���������
    m_hint.sin_family = AF_INET;
    m_hint.sin_port = htons(m_port);
    inet_pton(AF_INET, m_ipAddress.c_str(), &m_hint.sin_addr);
    //��������� IP ������ � port � �������
    int bindOk = bind(sock_server, (sockaddr*)& m_hint, sizeof(m_hint));
    if (bindOk != SOCKET_ERROR)
    {
      //���������� ���������, ��� ����� ��� �������������
      int listenOk = listen(sock_server, SOMAXCONN);
      if (listenOk == SOCKET_ERROR)
      {
        std::cerr << "Couldnt listen socket, error #" << WSAGetLastError() << std::endl;
        return SOCKET_ERROR;
      }
    }
    else {
      std::cerr << "Couldnt bind socket, error #" << WSAGetLastError() << std::endl;
      return SOCKET_ERROR;
    }

  }
  else {
    std::cerr << "Couldnt create socket, error #" << WSAGetLastError() << std::endl;
    if (strcmp(PLATFORM_NAME, "windows") == 0) {
      WSACleanup();
    }
  }

  return sock_server;
}

TCPServer::TCPServer(std::string ipAddress, int port, MessageRecievedHandler handler)
  : m_ipAddress(ipAddress), m_port(port), MessageReceived(handler)
{
  FD_ZERO(&master);
  m_run_thread_running = false;
}

TCPServer::~TCPServer() {
  //��������� ��� ������ � ������� � ��������� �������
  std::string msg = "Server is shutting down.\r\n";
  while (master.fd_count > 0)
  {
    SOCKET sock = master.fd_array[0];

    send(sock, msg.c_str(), (int)(msg.size() + 1), 0);

    FD_CLR(sock, &master);
    closesocket(sock);
  }
  //����� �������� �������
  if (strcmp(PLATFORM_NAME, "windows") == 0) {
    WSACleanup();
  }
  //������������� thread
  if (m_run_thread_running)
  {
    m_run_thread_running = false;
    std::cout << "Finishing RUN thread..." << std::endl;
    m_run_thread.join();
  }
}

bool TCPServer::Init()
{
  if (strcmp(PLATFORM_NAME, "windows") == 0) {
    WSAData data;
    WORD ver = MAKEWORD(2, 2);

    int wsInit = WSAStartup(ver, &data);
    if (wsInit != 0)
    {
      std::cerr << "Couldnt initialize winsock, error #" << wsInit << std::endl;
      return false;
    }

    return wsInit == 0;
  }
  else return true;
}

bool TCPServer::Send(SOCKET clientSocket, std::string message)
{
  if (!message.empty())
  {
    return send(clientSocket, message.c_str(), (int)(message.size() + 1), 0) != SOCKET_ERROR;
  }
  return false;
}

void TCPServer::SendToAllExcept(SOCKET clientSocket, std::string message)
{
  if (!message.empty())
  {
    //���������� ��������� ������ �������� � �� listening ������
    for (unsigned int i = 0; i < master.fd_count; i++)
    {
      SOCKET outSock = master.fd_array[i];
      if ((outSock != m_listening_sock) && (outSock != clientSocket))
      {
        send(outSock, message.c_str(), (int)(message.size() + 1), 0);
      }
    }


  }
}

void TCPServer::RunInThread()
{
  //������� thread ����� ������
  this->m_run_thread = std::thread([&]()
    {
      Run();
    });
}

void TCPServer::Run()
{
  //������� listening �����
  m_listening_sock = CreateSocket();
  if (m_listening_sock == INVALID_SOCKET)
  {
    std::cerr << "Error while starting, Couldnt start a server." << std::endl;
    return;
  }
  // ��������� ������ ������ ��� �����; listening �����
  // ��� ����� ������ �� �� ������ ��������� �������
  // ����������� 

  FD_SET(m_listening_sock, &master);

  m_run_thread_running = true;
  std::cout << "Server is running" << std::endl;
  while (m_run_thread_running)
  {
    //������� ����� ���� �����������, ��� ��� ����������� select ������������
    fd_set copy = master;
    //������� ��� � ���� ������������ �����������
    int socketCount = select(0, &copy, nullptr, nullptr, nullptr);
    //���������� �� ���� ������� ������������
    for (int i = 0; i < socketCount; i++)
    {
      SOCKET sock = copy.fd_array[i];
      //�������� �� �������� ������������
      if (sock == m_listening_sock)
      {
        //���������� ����� ������
        SOCKET client = accept(m_listening_sock, nullptr, nullptr);
        //��������� ����� ������ � ������ ������������ ��������
        FD_SET(client, &master);
        //���������� ��������� ������� ��� ������ �������
        std::string welcomeMsg = "Welcome to my kursovaya!\r\n";
        send(client, welcomeMsg.c_str(), (int)(welcomeMsg.size() + 1), 0);
      }
      else
      {
        char buf[4096];
        ZeroMemory(buf, 4096);

        int bytesIn = recv(sock, buf, 4096, 0);
        if (bytesIn <= 0)
        {
          closesocket(sock);
          FD_CLR(sock, &master);
        }
        else
        {
          //�������� ��������� �� ������� quit, ������� ������� ������
          if (buf[0] == '\\\r\n')
          {
            std::string cmd = std::string(buf, bytesIn);
            if (cmd == "\\quit\r\n")
            {
              m_run_thread_running = false;
              break;
            }

            continue;
          }

          if (MessageReceived != NULL)
          {
            MessageReceived(this, sock, std::string(buf, 0, bytesIn));
          }
        }
      }
    }
  }
  //������� listening ����� �� ���� ����������� ������� � ��������� ���, ����� ����� �� �����������

  FD_CLR(m_listening_sock, &master);
  closesocket(m_listening_sock);
  std::cout << "Server stopped" << std::endl;
  //���������� ������������� � �������� �������
  std::string msg = "Server is shutting down.\r\n";

  while (master.fd_count > 0)
  {
    SOCKET sock = master.fd_array[0];

    send(sock, msg.c_str(), (int)(msg.size() + 1), 0);
    //��������� ���������� ���� ��������
    FD_CLR(sock, &master);
    closesocket(sock);
  }

}