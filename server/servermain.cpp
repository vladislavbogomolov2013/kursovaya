#include <iostream>
#include <string>
#include "TCPListener.h"


//! @brief �������������� ��������� ������� ������ ��������
//! @param[in] listen_server - ���� ������
//! @param[in] client - �� ���� ������
//! @param[in] msg - ��� ������
void MessageReceived(TCPServer* listen_server, SOCKET client, std::string msg)
{
  listen_server->SendToAllExcept(client, msg);
  std::cout << msg << "\r";
}
int main()
{
  std::cout << "Current OS: " << PLATFORM_NAME << std::endl;
  system("color F0");
  std::string inputstr;
  TCPServer *server = new TCPServer("127.0.0.1", 54010, MessageReceived);

  if (server->Init())
  {
      server->RunInThread();

      while (true)
      {
          std::getline(std::cin, inputstr);
          if (inputstr == "exit") break;

          inputstr += "\n";
          server->SendToAllExcept(NULL, inputstr);
      }
  }
  else {
      std::cout << "Fail to init server." << std::endl;
  }

  delete server; 

  std::cin.get();
  return 0;
}