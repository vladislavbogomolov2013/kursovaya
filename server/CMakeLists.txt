add_executable(server Realisation.cpp servermain.cpp TCPListener.h)
if(WIN32)
	target_link_libraries(server wsock32 ws2_32)
endif()