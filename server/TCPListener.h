#if defined(_WIN32)
  #define PLATFORM_NAME "windows" // Windows
  #include <WS2tcpip.h>
#elif defined(__unix__)
  #define PLATFORM_NAME "unix" // Unix systems
  #include <sys/socket.h>
  #include <arpa/inet.h>
  #include <sys/types.h>
  #define SOCKET int
  #define closesocket(x) (close(x))
  #define WSAGetLastError() (strerror())
  #define ZeroMemory(x,y) (memset(x, 0, y))
// ������� ��������� ������� ���������� ������ �� �� windosws � ��������� ��� �� unix
#endif
#include <string>
#include <iostream>
#include <thread>

class TCPServer;
//Callback �� ���������� ������
typedef void(*MessageRecievedHandler)(TCPServer* listener, SOCKET socketId, std::string msg);

//! @brief �������� ��������� ��� ������ TCPServer
class TCPServer {
private:
  std::string     m_ipAddress;            // IP Address �������
  int             m_port;                 // Listening port # �������
  sockaddr_in     m_hint;
  bool            m_run_thread_running;   // �������� �� ���������� Thread'a
  std::thread     m_run_thread;
  SOCKET          m_listening_sock;

  fd_set          master;

  MessageRecievedHandler  MessageReceived;
  //! @brief ������� ����� ��� send/recv
  //! @returns SOCKET �� ws2_32.lib
  SOCKET CreateSocket();

public:

  TCPServer(std::string ipAddress, int port, MessageRecievedHandler handler);
  //! @brief ���������� ��������� ������� ������������ � ���������� �� ��������� � �������� �������
  ~TCPServer();
  //! @brief �������������� winsocket
  //! @returns false ��� true � ����������� �� ���������� �������������
  bool Init();
  //! @brief ���������� ��������� ������ �������
  //! @param[in] clientSocket - ���� �� ����������
  //! @param[in] message - ��� �� ����������
  bool Send(SOCKET clientSocket, std::string message);
  //! @brief ���������� ��������� ���� �������������, ����� ������(������ ����� ������ �����������)
  //! ���� ��������� ���������� ������, �� ������ clientsocket ����������� �������� NULL
  //! @param[in] clientSocket - ���� �� �� ����������
  //! @param[in] message - ��� �� ����������
  void SendToAllExcept(SOCKET clientSocket, std::string message);
  //! @brief ��������� ������������ ���������� ��������
  void RunInThread();
  //! @brief ��������� ������
  void Run();

};