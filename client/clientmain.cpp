#include "tcpclient.h"
#include <string>
#include <iostream>
//! @brief ������� ���������� ��������� �� �������
//! @param[in] msg_received - ���������� ���������
void MessageReceived(std::string msg_received) {
  std::cout << "MSG FROM SERVER> " << msg_received << std::endl;
}

char client_name[20];
std::string message;

int main() {
  std::cout << "Current OS: " << PLATFORM_NAME << std::endl;
  system("color F0");
  std::string inputstr;
  TCPClient* client = new TCPClient;
  char adrserv[INET_ADDRSTRLEN];
  int port;
  FILE* a = fopen("info.txt", "r");
  if (!a)
    return 321;
  printf("Inputed port: ");
  fscanf(a, "%d\n", &port);
  std::cout << port << std::endl;
  printf("Inputed ip: ");
  fscanf(a, "%s\n", &adrserv);
  std::cout << adrserv << std::endl;
  printf("Inputed name: ");
  fscanf(a, "%s\n", &client_name);
  std::cout << client_name << std::endl;
  strcat(client_name, ": ");
  fclose(a);
  
  if (client->Connect(adrserv, port))
  {
    client->ListenRecvInThread(MessageReceived);

    while (true)
    {
      std::getline(std::cin, inputstr);
      if (inputstr == "exit") break;
      message = client_name + inputstr;
      client->Send(message);
    }
  }
  else {
      std::cout << "Connect() fail." << std::endl;
  }

  delete client;


  std::cin.get(); 
  return 0;
}
