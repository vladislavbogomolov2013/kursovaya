#include "tcpclient.h"

bool TCPClient::Init()
{
  if (strcmp(PLATFORM_NAME,"windows") == 0) {
    WSAData data;
    WORD ver = MAKEWORD(2, 2);

    int wsInit = WSAStartup(ver, &data);
    if (wsInit != 0)
    {
      std::cerr << "Can't start Winsock, Err #" << wsInit << std::endl;
      return false;
    }

    return wsInit == 0;
  }
  else return true;
}

SOCKET TCPClient::CreateSocket()
{
  SOCKET sock_client = socket(AF_INET, SOCK_STREAM, 0);
  if (sock_client != INVALID_SOCKET)
  {
    m_hint.sin_family = AF_INET;
    m_hint.sin_port = htons(m_port);
    inet_pton(AF_INET, m_ipAddress.c_str(), &m_hint.sin_addr);
  }
  else {
    std::cerr << "Can't create socket, Err #" << WSAGetLastError() << std::endl;
    if (strcmp(PLATFORM_NAME, "windows") == 0) {
      WSACleanup();
    }
  }

  return sock_client;
}

void TCPClient::ThreadRecv()
{
  m_recv_thread_running = true;
  while (m_recv_thread_running)
  {
    char buf[4096];
    ZeroMemory(buf, 4096);

    int bytesReceived = recv(m_socket, buf, 4096, 0);
    if (bytesReceived > 0)
    {
      if (MessageReceived != NULL)
      {
        MessageReceived(std::string(buf, 0, bytesReceived));
      }
    }
  }
}

TCPClient::TCPClient() {
  m_socket = INVALID_SOCKET;
  m_recv_thread_running = false;
}

TCPClient::~TCPClient() {
  closesocket(m_socket);
  if (strcmp(PLATFORM_NAME, "windows") == 0) {
    WSACleanup();
  }
  if (m_recv_thread_running)
  {
    m_recv_thread_running = false;
    m_recv_thread.join();
  }
}

bool TCPClient::Connect(std::string ipAddress, int port)
{
  m_ipAddress = ipAddress;
  m_port = port;

  if (!Init()) return false;

  m_socket = CreateSocket();
  if (m_socket == INVALID_SOCKET) return false;

  int connResult = connect(m_socket, (sockaddr*)& m_hint, sizeof(m_hint));
  if (connResult == SOCKET_ERROR)
  {
    std::cerr << "Can't connect to server, Err #" << WSAGetLastError() << std::endl;
    return false;
  }
  return true;
}

bool TCPClient::Send(std::string message)
{
  if (!message.empty() && m_socket != INVALID_SOCKET)
  {
    return send(m_socket, message.c_str(), message.size() + 1, 0) != SOCKET_ERROR;
  }
  return false;
}

void TCPClient::ListenRecvInThread(MessageRecievedHandler handler)
{
  MessageReceived = handler;
  std::thread recv_t(&TCPClient::ThreadRecv, this);
  this->m_recv_thread = std::move(recv_t);

}

bool TCPClient::Recv(MessageRecievedHandler handler)
{
  MessageReceived = handler;

  if (m_socket == INVALID_SOCKET) return false;

  char buf[4096];

  ZeroMemory(buf, 4096);
  int bytesReceived = recv(m_socket, buf, 4096, 0);
  if (bytesReceived > 0)
  {
    if (MessageReceived != NULL)
    {
      MessageReceived(std::string(buf, 0, bytesReceived));
    }

    std::cout << "SERVER> " << std::string(buf, 0, bytesReceived) << std::endl;
  }
  return true;
}
