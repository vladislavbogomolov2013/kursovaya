#if defined(_WIN32)
  #define PLATFORM_NAME "windows" // Windows
  #include <WS2tcpip.h>
#elif defined(__unix__)
  #define PLATFORM_NAME "unix" // Unix systems
  #include <sys/socket.h>
  #include <arpa/inet.h>
  #include <sys/types.h>
  #define SOCKET int
  #define closesocket(x) (close(x))
  #define WSAGetLastError() (strerror())
  #define ZeroMemory(x,y) (memset(x, 0, y))
// ������� ��������� ������� ���������� ������ �� �� windosws � ��������� ��� �� unix
#endif
#include <string>
#include <iostream>
#include <thread>


class TCPClient;

typedef void(*MessageRecievedHandler)(std::string msg);

//! @brief �������� ��������� ��� ������ TCPClient
class TCPClient {
private:
  std::string     m_ipAddress;            
  int             m_port;                 
  SOCKET          m_socket;
  sockaddr_in     m_hint;
  bool            m_recv_thread_running; 
  std::thread     m_recv_thread;

  MessageRecievedHandler  MessageReceived;
  //! @brief �������������� winsocket
  bool Init();
  //! @brief ������� ����� ��� send/recv
  //! @returns SOCKET �� ws2_32.lib
  SOCKET CreateSocket();
  //! @brief ������ ������ �� SOCKET � �������������� Thread
  void ThreadRecv();

public:

  TCPClient();

  ~TCPClient();

  //! @brief ������������ � �������
  //! @param[in] ipAddress - ip �������
  //! @param[in] port - port �������
  bool Connect(std::string ipAddress, int port);
  //! @brief ���������� ��������� �� ������
  //! @param[in] message - ��� ����������
  bool Send(std::string message);
  //! @brief �������� ��������� �� �������
  void ListenRecvInThread(MessageRecievedHandler handler);
  //! @brief �������� ��������� �� �������
  bool Recv(MessageRecievedHandler handler);

};